#!/bin/bash

. "${0%/*}/bash/functions.sh"

if [ "${1}" == "--help" ] || [ "${1}" == "-h" ]; then
    usage
    exit 1
fi;

CODE_PATH="$(dirname $0)/../"
CODE_PATH="$(abspath ${CODE_PATH})/"

#
# COMMAND=build|branch|release
#
# build   - build images for local development
# branch  - build images and push to registry for deploy on branch on test
# release - build images and push to registry for deploy on test and production
#

for i in "$@"; do
    case $i in
        --command=*)
        COMMAND="${i#*=}"
        shift
        ;;
        --release=*)
        RELEASE="${i#*=}"
        shift
        ;;        
        --services=*)
        SERVICES="${i#*=}"
        shift # past argument=value
        ;;
        *)
        # unknown option
        ;;
    esac
done

COMMAND="${COMMAND:-build}"
RELEASE="${RELEASE:-latest}"

if [ -z $SERVICES ]; then
    SERVICES=(`ls ${CODE_PATH}/deploy/docker/`)
else
    IFS=', ' read -r -a SERVICES <<< "$SERVICES"
fi;

REGISTRY="192.168.14.50:5000"
REGISTRY_USERNAME="_"
REGISTRY_PASSWORD="_"

IMAGE_PREFIX=""

echo "Ready for image building"
echo
echo "COMMAND      : ${COMMAND}"
echo "CODE_PATH    : ${CODE_PATH}"
echo "RELEASE TAG  : ${RELEASE}"
echo "REGISTRY     : ${REGISTRY}"
echo "IMAGE_PREFIX : ${IMAGE_PREFIX}"
echo

if [ "${COMMAND}" != "build" ]; then
    docker login -u ${REGISTRY_USERNAME} -p ${REGISTRY_PASSWORD} ${REGISTRY}
fi;

cd ${CODE_PATH} || exit 1

for SERVICE in "${SERVICES[@]}"; do :

    DOCKERFILE="${CODE_PATH}/deploy/docker/${SERVICE}/Dockerfile"

    if [ -f ${DOCKERFILE} ]; then
        echo "SERVICE: ${SERVICE}"
        BUILD_ARGS=""

        IMAGE="${IMAGE_PREFIX}${SERVICE}:${RELEASE}"

        case ${SERVICE} in           
            "bus"*)
              BUILD_ARGS="--build-arg DEPLOY_PATH=./deploy/docker/${SERVICE}"
              ;;
            "wm"*)
              BUILD_ARGS="--build-arg DEPLOY_PATH=./deploy/docker/${SERVICE}"
              ;;                      
        esac

        docker build ${BUILD_ARGS} -t ${IMAGE} . -f ${DOCKERFILE} || exit 1

        if [ "${COMMAND}" != "build" ]; then

            docker tag ${IMAGE} ${REGISTRY}/${IMAGE} || exit 1

            docker push ${REGISTRY}/${IMAGE} || exit 1

        fi;
    fi;

done