const express = require('express');
const app = express();
const ecommpay = require('./api/v1.0/payment-systems/ecommpay');
const config = require('./config');
const port = process.env.PORT || config.port || 8888;

process.on('uncaughtException', (error) => {
    console.error(error);
});

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
    res.header('Content-Type', 'application/json');
  
    if (req.method === 'OPTIONS') {
        return res.status(200).end();
    }

	next();
});

app.use('/bill/api/v1.0/payment-systems/ecommpay', ecommpay);

app.get('/', (req, res) => {
    res.redirect(config.qtbusEndpoint + req.originalUrl);
});

app.listen(port, () => console.log(`Server listening on port ${port}`)).setTimeout(config.timeout || 0);
