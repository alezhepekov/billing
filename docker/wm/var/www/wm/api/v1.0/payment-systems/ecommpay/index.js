const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const http = require('http');
const https = require('https');
const url = require('url');
const crypto = require('crypto');
const ECP = require('ecommpay');
const signer = require('ecommpay/src/signer');
const Callback = require('./callback');
const config = require('../../../../config');

router.use((req, res, next) => {
    // res.connection.setTimeout(120000);
	next();
});

router.use(bodyParser.json());

router.post('/', (req, res) => {
    console.log('POST /bill/api/v1.0/payment-systems/ecommpay');
    console.log(req.url);
    console.log(JSON.stringify(req.body));

	let ftx = (req, res) => {
        // Payout
        if (req.body.operation.type && req.body.operation.type === 'payout') {
            console.log('operation.type: \"payout\"');

            let processPayoutOperation = () => {
                const callback = new Callback(config.ecommpay.salt, JSON.stringify(req.body));
                const callbackSignatureValidationResult = callback.isValid() ? 1 : 0;

                let requestPayoutRetringCount = 0;

                ((req, res, requestPayoutRetringCount, callbackSignatureValidationResult) => {
                    let runRequestPayout = () => {
                        const path = 
                            encodeURI('/?func=cb-wo-client-balance&params=' + JSON.stringify({
                                'app_call': 'Payout',
                                'external_id': req.body.payment.id,
                                'payment_system_id': 150,
                                'session': {
                                    'ORDER_DT': {
                                        'AmountPS': req.body.operation.sum_initial.amount,
                                        'CurrCode_S': req.body.operation.sum_initial.currency,
                                        'PSPayment_State': req.body.payment.status,
                                        'PSValid_Error_Msg': req.body.hasOwnProperty('errors') ? JSON.stringify(req.body.errors) : '',
                                        'PSOperation_State': req.body.operation.status,
                                        'FOperationCode': req.body.operation.hasOwnProperty('code') ? req.body.operation.code : '',
                                        'FOperationMsg': req.body.operation.hasOwnProperty('message') ? req.body.operation.message : '',
                                        'Hash_Check': callbackSignatureValidationResult,
                                        'FRequestText': JSON.stringify(req.body),
                                        'FResponseText': '200'
                                    }
                                }
                            }));

                        const options = {
                            host: config.qtbusEndpoint.host,
                            port: config.qtbusEndpoint.port,
                            path: path,
                            headers: {
                                'Connection': 'close'
                            },
                            timeout: 150000
                        };

                        console.log(`url: http://${options.host}:${options.port}${options.path}`);
                
                        let request = http.get(options, (subRes) => {
                            const { statusCode } = subRes;
                            const contentType = subRes.headers['content-type'];
                            let error = null;
                            if (statusCode === 408) {
                                const error = new Error('Timeout');
                                request.emit('error', error);
                            } else if (statusCode !== 200) {
                                error = new Error(`runRequestPayout: Request Failed. Status Code: ${statusCode}`);
                            } else if (!/^application\/json/.test(contentType)) {
                                error = new Error(`runRequestPayout: Invalid content-type. Expected application/json but received ${contentType}`);
                            }
                
                            if (error) {
                                console.error(error.message);
                                subRes.resume();
                                return;
                            }
                
                            subRes.setEncoding('utf8');
                            let rawData = '';
                            subRes.on('data', (chunk) => { rawData += chunk; });
                            subRes.on('end', () => {
                                console.log(`runRequestPayout: response data: ${rawData}`);
                                
                                try {
                                    const parsedData = JSON.parse(rawData);

                                    if (parsedData.hasOwnProperty('error') && parsedData['error'].hasOwnProperty('code') && parsedData['error']['code'] === 'ERR_SCRIPT_LOADIND') {
                                        // Повторный запрос в случае ошибки загрузки скрипта
                                        const error = new Error(parsedData['error']['message']);
                                        request.emit('error', error);
                                        return;
                                    }
                                    
                                    if (parsedData.hasOwnProperty('error') && parsedData['error'].hasOwnProperty('code') && parsedData['error']['code'] === 'ERR_SCRIPT_RUNTIME') {
                                        // Повторный запрос в случае ошибки выполнения скрипта
                                        const error = new Error(parsedData['error']['message']);
                                        request.emit('error', error);
                                        return;
                                    }

                                    const session = JSON.parse(parsedData.data.session);
    
                                    const responseStatusCode = parseInt(session.SESS_DT_OUT.ResponseText || '200');
                                    res.status(responseStatusCode).end(); // Статус определяется на основе возвращенного значения
                                } catch (e) {
                                    const message = `runRequestPayout: problem with request: ${e.message}`;
                                    console.error(message);
                                    res.status(500).end(JSON.stringify({
                                        error: {
                                            message: message
                                        }
                                    }));
                                }
                            });
                        }).on('error', (e) => {
                            console.error(`runRequestPayout: problem with request: ${e.message}`);
                            
                            if (requestPayoutRetringCount >= config.requestRetryCountLimit) {
                                const message = `runRequestPayout: retry limit reached. requestRetryCountLimit: ${config.requestRetryCountLimit}`;
                                console.error(`message: ${message}`);
                                res.status(500).end(JSON.stringify({
                                    error: {
                                        message: message
                                    }
                                }));
                            }
                            else {
                                requestPayoutRetringCount++;
                                console.log(`Retring runRequestPayout:... requestRetringCount: ${requestPayoutRetringCount}`);
                                runRequestPayout();
                            }
                        });
                    }
            
                    runRequestPayout();
                })(req, res, requestPayoutRetringCount, callbackSignatureValidationResult);
            }

            processPayoutOperation();
            return;
        }

        // Payin
        let processPayinOperation = () => {
            console.log('operation.type: \"payin\"');

            let aFProcessingCode = 'I01';

            const callback = new Callback(config.ecommpay.salt, JSON.stringify(req.body));

            if (!callback.isValid()) {
                const message = 'Некорректный ответ ECommPay';
                res.status(400).end(JSON.stringify({
                    error: {
                        message: message
                    }
                }));
                aFProcessingCode = 'E01';
                fFResponseText = '400';
                console.error(`Error: ${message}`);
                return;
            }

            const paymentData = req.body;
        
            let aPaymentSystemId = 0;
            if (paymentData.payment.method === 'card') {
                aPaymentSystemId = 150;
            }
        
            const fAmount = (paymentData.payment.sum.amount / 100).toFixed(2);
            const fAmountPS = (paymentData.operation.sum_converted.amount / 100).toFixed(2);
            const fCurrCode_S = paymentData.operation.sum_converted.currency;
            const aExternal_Id = paymentData.payment.id;
            let fFPaymentInfo = paymentData.account.number;

            if (aPaymentSystemId === 150) {
                fFPaymentInfo = fFPaymentInfo.replace(/\*/g, 'X');
            }

            const fPSState = paymentData.payment.status === 'success' ? 'soOK' : 'soError';
            const fHash = paymentData.signature;
            const fCard_Token = paymentData.account.token;
            const fFRequestText = paymentData;
        
            if (fPSState === 'soError') {
                const message = 'Неверный статус';
                res.status(400).end(JSON.stringify({
                    error: {
                        message: message
                    }
                }));
                aFProcessingCode = 'E02';
                fFResponseText = '400';
                console.error(`Error: ${message}`);
                return;
            }

            if (!fAmount) {
                const message = 'Сумма не найдена';
                res.status(400).end(JSON.stringify({
                    error: {
                        message: message
                    }
                }));
                aFProcessingCode = 'E03';
                fFResponseText = '400';
                console.error(`Error: ${message}`);
                return;
            }

            if (!fAmountPS) {
                const message = 'Сумма не найдена';
                res.status(400).end(JSON.stringify({
                    error: {
                        message: message
                    }
                }));
                aFProcessingCode = 'E03';
                fFResponseText = '400';
                console.error(`Error: ${message}`);
                return;
            }

            if (!fCurrCode_S) {
                const message = 'Валюта не найдена';
                res.status(400).end(JSON.stringify({
                    error: {
                        message: message
                    }
                }));
                aFProcessingCode = 'E04';
                fFResponseText = '400';
                console.error(`Error: ${message}`);
                return;
            }

            if (!aExternal_Id) {
                const message = 'Идентификатор GET-запроса не найден';
                res.status(400).end(JSON.stringify({
                    error: {
                        message: message
                    }
                }));
                aFProcessingCode = 'E05';
                fFResponseText = '400';
                console.error(`Error: ${message}`);
                return;
            }

            if (!fFPaymentInfo) {
                const message = 'Платёжные реквизиты не найдены';
                res.status(400).end(JSON.stringify({
                    error: {
                        message: message
                    }
                }));
                aFProcessingCode = 'E06';
                fFResponseText = '400';
                console.error(`Error: ${message}`);
                return;
            }

            let requestPayinRetringCount = 0;

            ((req, res, requestPayinRetringCount, aExternal_Id, aPaymentSystemId, aFProcessingCode, fFPaymentInfo, fAmount, fAmountPS, fCurrCode_S, fPSState, fHash, fCard_Token, fFRequestText) => {
                let runRequestPayin = () => {
                    const path = 
                        encodeURI('/?func=refill-client-balance&params=' + JSON.stringify({
                            'app_call': 'billing',
                            'external_id': aExternal_Id,
                            'payment_system_id': aPaymentSystemId,
                            'processing_code': aFProcessingCode,
                            'session': {
                                'SESS_DATA': {
                                    'FPaymentInfo': fFPaymentInfo,
                                    'Amount': fAmount,
                                    'AmountPS': fAmountPS,
                                    'CurrCode_S': fCurrCode_S,
                                    'Transaction_Id': aExternal_Id,
                                    'PSState': fPSState,
                                    'FWithdrawApproval': 0,
                                    'Hash': fHash,
                                    'Card_Token': fCard_Token,
                                    'FRequestText': fFRequestText
                                }
                            }
                        }));

                    const options = {
                        host: config.qtbusEndpoint.host,
                        port: config.qtbusEndpoint.port,
                        path: path,
                        headers: {
                            'Connection': 'close'
                        },
                        timeout: 150000
                    };

                    console.log(`url: http://${options.host}:${options.port}${options.path}`);
            
                    let request = http.get(options, (subRes) => {
                        const { statusCode } = subRes;
                        const contentType = subRes.headers['content-type'];
                        let error = null;
                        if (statusCode === 408) {
                            const error = new Error('Timeout');
                            request.emit('error', error);
                        } else if (statusCode !== 200) {
                            error = new Error(`runRequestPayin: Request Failed. Status Code: ${statusCode}`);
                        } else if (!/^application\/json/.test(contentType)) {
                            error = new Error(`runRequestPayin: Invalid content-type. Expected application/json but received ${contentType}`);
                        }
            
                        if (error) {
                            console.error(error.message);
                            subRes.resume();
                            return;
                        }
            
                        subRes.setEncoding('utf8');
                        let rawData = '';
                        subRes.on('data', (chunk) => { rawData += chunk; });
                        subRes.on('end', () => {
                            console.log(`runRequestPayin: response data: ${rawData}`);

                            try {
                                const parsedData = JSON.parse(rawData);

                                if (parsedData.hasOwnProperty('error') && parsedData['error'].hasOwnProperty('code') && parsedData['error']['code'] === 'ERR_SCRIPT_LOADIND') {
                                    // Повторный запрос в случае ошибки загрузки скрипта
                                    const error = new Error(parsedData['error']['message']);
                                    request.emit('error', error);
                                    return;
                                }

                                if (parsedData.hasOwnProperty('error') && parsedData['error'].hasOwnProperty('code') && parsedData['error']['code'] === 'ERR_SCRIPT_RUNTIME') {
                                    // Повторный запрос в случае ошибки выполнения скрипта
                                    const error = new Error(parsedData['error']['message']);
                                    request.emit('error', error);
                                    return;
                                }
                                
                                const session = JSON.parse(parsedData.data.session);
                                
                                const responseStatusCode = parseInt(session.SESS_DT_OUT.ResponseText || '200');
                                res.status(responseStatusCode).end(); // Статус определяется на основе возвращенного значения
                            } catch (e) {
                                const message = `runRequestPayin: problem with request: ${e.message}`;
                                console.error(message);
                                res.status(500).end(JSON.stringify({
                                    error: {
                                        message: message
                                    }
                                }));
                            }
                        });
                    }).on('error', (e) => {
                        console.error(`runRequestPayin: problem with request: ${e.message}`);
                    
                        if (requestPayinRetringCount >= config.requestRetryCountLimit) {
                            const message = `runRequestPayin: retry limit reached. requestRetryCountLimit: ${config.requestRetryCountLimit}`;
                            console.error(`message: ${message}`);
                            res.status(500).end(JSON.stringify({
                                error: {
                                    message: message
                                }
                            }));
                        }
                        else {
                            requestPayinRetringCount++;
                            console.log(`Retring runRequestPayin:... requestRetringCount: ${requestPayinRetringCount}`);
                            runRequestPayin();
                        }
                    });
                }
    
                runRequestPayin();
            })(req, res, requestPayinRetringCount, aExternal_Id, aPaymentSystemId, aFProcessingCode, fFPaymentInfo, fAmount, fAmountPS, fCurrCode_S, fPSState, fHash, fCard_Token, fFRequestText);
        }
        
        processPayinOperation();
    };

    ftx(req, res);
});

function buildQuery(paymentData) {
    const signature = encodeURIComponent(signer(paymentData.params, paymentData.salt));
    const params = Object.keys(paymentData.params).map(key => `${key}=${encodeURIComponent(paymentData.params[key])}`).join('&');

    return `${paymentData.baseURI}/payment?${params}&signature=${signature}`;
}

router.post('/notifications', (req, res) => {
    console.log('POST /bill/api/v1.0/payment-systems/ecommpay/notifications');
    console.log(req.url);
    console.log(JSON.stringify(req.body));

    let ftx = (req, res) => {
        res.status(200).end();
    };

    ftx(req, res);
});

router.get('/test', (req, res) => {
    console.log('GET /bill/api/v1.0/payment-systems/ecommpay/test');
    console.log(req.url);

	let ftx = (req, res) => {
        const project_id = req.query.project_id;
        const salt = req.query.salt;
        const payment_amount = req.query.payment_amount;
        const payment_id = req.query.payment_id;
        const payment_currency = req.query.payment_currency;
        const customer_id = req.query.customer_id;
        const card_operation_type = req.query.card_operation_type;
        const language_code = req.query.language_code;
        
        if (
            !project_id
            || !salt
            || !payment_amount
            || !payment_id
            || !payment_currency
            || !customer_id
            || !card_operation_type
            || !language_code) {
            res.status(400).end(JSON.stringify({
                error: {
                    message: 'Invalid input parameters'
                }
            }));
            return;
        }

        const payment = new ECP(project_id, salt);
        
        payment.paymentAmount = payment_amount;
        payment.paymentId = payment_id;
        payment.paymentCurrency = payment_currency;
        payment.customerId = customer_id;
        payment.cardOperationType = card_operation_type;
        payment.languageCode = language_code;

        const urlx = buildQuery(payment);
        console.log(urlx);
        res.redirect(urlx);
    };

    ftx(req, res);
});

router.get('/testx', (req, res) => {
    console.time('testx');
    console.log('GET /bill/api/v1.0/payment-systems/ecommpay/testx');
    console.log(req.url);
    console.log(JSON.stringify(req.body));

	let ftx = (req, res) => {
        const clientId = req.query.clientId;
        const paymentSystemId = req.query.paymentSystemId;
        const amount = req.query.amount;
        const subData = req.query.subData;
        const clientOperationId = req.query.clientOperationId;

        const urlx = config._qtbusEndpoint + '/?func=cash-in&params=' + JSON.stringify({
            FClientId: clientId,
            FPaymentSystemId: paymentSystemId,
            FAmount: amount,
            FSubData: subData,
            FClientOperationId: clientOperationId
        });

        console.log(urlx);

        http.get(urlx, (subRes) => {
            const { statusCode } = subRes;
            const contentType = subRes.headers['content-type'];
            let error = null;
            if (statusCode !== 200) {
                error = new Error(`Error query-0: Request Failed. Status Code: ${statusCode}`);
            } else if (!/^application\/json/.test(contentType)) {
                error = new Error(`Error query-0: Invalid content-type. Expected application/json but received ${contentType}`);
            }

            if (error) {
                console.error(error.message);
                subRes.resume();
                return;
            }

            subRes.setEncoding('utf8');
            let rawData = '';
            subRes.on('data', (chunk) => { rawData += chunk; });
            subRes.on('end', () => {
                console.log(rawData);
                try {
                    const parsedData = JSON.parse(rawData);

                    if (!parsedData['result'] || parsedData['result'] == false) {
                        res.status(500).end(JSON.stringify({
                            error: {
                                message: parsedData['error'] || 'Error'
                            }
                        }));
                        console.error(parsedData['error'] || 'Error');
                        return;
                    }

                    console.log(parsedData['data']['url']);
                    // Вызов страницы платежа Ecommpay
                    if (!parsedData['data']['url']) {
                        res.status(500).end(JSON.stringify({
                            error: {
                                message: parsedData['error'] || 'The received data does not contain the desired parameter - url.'
                            }
                        }));
                        console.error(parsedData['error'] || 'The received data does not contain the desired parameter - url.');
                        return;
                    }
                    res.redirect(parsedData['data']['url']);
                } catch (e) {
                    console.error(e.message);
                }
            });
        }).on('error', (e) => {
            console.error(e.message);
        });
    };

    ftx(req, res);
});

router.post('/testz', (req, res) => {
    console.time('testz');
    console.log('POST /bill/api/v1.0/payment-systems/ecommpay/testz');
    console.log(req.url);

	let ftx = (req, res) => {
        // Ограничение количества выполняемых запросов
        const numberOfRequestsToExecute = parseInt(req.query.numberOfRequestsToExecute) || 10;
        
        // Интервал выполнения запросов
        const queryInterval = req.query.queryInterval || 55;
      
        let stopFlag = false;
        let i = -1;
        let j = 0;
        const intervalId = setInterval(() => {
            if ((++i >= numberOfRequestsToExecute) && !stopFlag) {
                clearInterval(intervalId);
                return;
            }

            ((i) => {
                console.time(`query-${i}`);

                const postData = JSON.stringify(req.body);

                const options = {
                    host: config.apiEndpoint.host,
                    port: config.apiEndpoint.port,
                    path: '/bill/api/v1.0/payment-systems/ecommpay',
                    method: 'POST',
                    headers: {
                        'content-type': 'application/json',
                        'content-length': Buffer.byteLength(postData)
                    }
                };

                const subReq = http.request(options, (subRes) => {
                    const query = `${subRes.req.method} ${subRes.socket.remoteAddress}:${subRes.socket.remotePort}${subRes.req.path}`;
                    console.log(`query-${i}: query: ${query}`);
                    console.log(`query-${i}: status: ${subRes.statusCode}`);
                    // console.log(`query-${i}: headers: ${JSON.stringify(subRes.headers)}`);
                    subRes.setEncoding('utf8');
                    let rawData = '';
                    subRes.on('data', (chunk) => { rawData += chunk; });
                    subRes.on('end', () => {
                        console.log(rawData);
                        console.log(`query-${i}: finished!`)
                        console.timeEnd(`query-${i}`);
                        if (++j >= numberOfRequestsToExecute) {
                            res.status(200).end();
                            console.timeEnd('testz');
                        }
                    });
                });

                subReq.on('error', (e) => {
                    console.error(`query-${i}: problem with request: ${e.message}`);
                    console.timeEnd(`query-${i}`);
                    res.status(500).end(JSON.stringify({
                        error: {
                            message: e.message
                        }
                    }));
                    console.timeEnd('testz');
                    stopFlag = true;
                });

                subReq.write(postData);
                subReq.end();
            })(i);
        }, queryInterval);
    };

    ftx(req, res);
});

router.post('/testz2', (req, res) => {
    console.time('testz2');
    console.log('POST /bill/api/v1.0/payment-systems/ecommpay/testz2');
    console.log(req.url);

    let ftx = (req, res) => {
        // Количество повторений
        const numberOfRepetitions = parseInt(req.query.numberOfRepetitions) || 1;

        // Массив с калбек-данными платежей
        const payments = req.body.payments;

        let stopFlag = false;
        let numberOfQueriesExecuted = 0;
        for (let j = 0; j < numberOfRepetitions && !stopFlag; j++) {
            for (let i = 0; i < payments.length && !stopFlag; i++) {
                const currentPayment = payments[i];
    
                ((i) => {
                    console.time(`query-${j}-${i}`);
    
                    const postData = JSON.stringify(currentPayment);
    
                    const options = {
                        host: config.apiEndpoint.host,
                        port: config.apiEndpoint.port,
                        path: '/bill/api/v1.0/payment-systems/ecommpay',
                        method: 'POST',
                        headers: {
                            'content-type': 'application/json',
                            'content-length': Buffer.byteLength(postData)
                        }
                    };
    
                    const subReq = http.request(options, (subRes) => {
                        const query = `${subRes.req.method} ${subRes.socket.remoteAddress}:${subRes.socket.remotePort}${subRes.req.path}`;
                        console.log(`query-${j}-${i}: query: ${query}`);
                        console.log(`query-${j}-${i}: status: ${subRes.statusCode}`);
                        // console.log(`query-${j}-${i}: headers: ${JSON.stringify(subRes.headers)}`);
                        subRes.setEncoding('utf8');
                        let rawData = '';
                        subRes.on('data', (chunk) => { rawData += chunk; });
                        subRes.on('end', () => {
                            console.log(rawData);
                            console.log(`query-${j}-${i}: finished!`);
                            console.timeEnd(`query-${j}-${i}`);
                            if (++numberOfQueriesExecuted === payments.length * numberOfRepetitions) {
                                res.status(200).end();
                                console.timeEnd('testz2');
                            }
                        });
                    });
    
                    subReq.on('error', (e) => {
                        console.error(`query-${j}-${i}: problem with request: ${e.message}`);
                        console.timeEnd(`query-${j}-${i}`);
                        res.status(500).end(JSON.stringify({
                            error: {
                                message: e.message
                            }
                        }));
                        console.timeEnd('testz2');
                        stopFlag = true;
                    });
    
                    subReq.write(postData);
                    subReq.end();
                })(i);
            }
        }
    };

    ftx(req, res);
});

function formatNumberLength(num, length) {
    let r = '' + num;
    while (r.length < length) {
        r = '0' + r;
    }
    return r;
}

router.post('/testz3', (req, res) => {
    console.time('testz3');
    console.log('POST /bill/api/v1.0/payment-systems/ecommpay/testz3');
    console.log(req.url);

    let ftx = (req, res) => {
        // Количество повторений
        const numberOfRepetitions = parseInt(req.query.numberOfRepetitions) || 1;

        // Массив с калбек-данными платежей
        const payments = req.body.payments;

        let stopFlag = false;
        let numberOfQueriesExecuted = 0;
        for (let j = 0; j < numberOfRepetitions && !stopFlag; j++) {
            for (let i = 0; i < payments.length && !stopFlag; i++) {
                const currentPayment = payments[i];

                currentPayment.payment.id = formatNumberLength(j + i * payments.length, 8);
                delete currentPayment.signature;
                const signature = signer(currentPayment, config.ecommpay.salt);
                currentPayment.signature = signature;
    
                ((i) => {
                    console.time(`query-${j}-${i}`);
    
                    const postData = JSON.stringify(currentPayment);
    
                    const options = {
                        host: config.apiEndpoint.host,
                        port: config.apiEndpoint.port,
                        path: '/bill/api/v1.0/payment-systems/ecommpay',
                        method: 'POST',
                        headers: {
                            'content-type': 'application/json',
                            'content-length': Buffer.byteLength(postData)
                        }
                    };
    
                    const subReq = http.request(options, (subRes) => {
                        const query = `${subRes.req.method} ${subRes.socket.remoteAddress}:${subRes.socket.remotePort}${subRes.req.path}`;
                        console.log(`query-${j}-${i}: query: ${query}`);
                        console.log(`query-${j}-${i}: status: ${subRes.statusCode}`);
                        // console.log(`query-${j}-${i}: headers: ${JSON.stringify(subRes.headers)}`);
                        subRes.setEncoding('utf8');
                        let rawData = '';
                        subRes.on('data', (chunk) => { rawData += chunk; });
                        subRes.on('end', () => {
                            console.log(rawData);
                            console.log(`query-${j}-${i}: finished!`);
                            console.timeEnd(`query-${j}-${i}`);
                            if (++numberOfQueriesExecuted === payments.length * numberOfRepetitions) {
                                res.status(200).end();
                                console.timeEnd('testz3');
                            }
                        });
                    });
    
                    subReq.on('error', (e) => {
                        console.error(`query-${j}-${i}: problem with request: ${e.message}`);
                        console.timeEnd(`query-${j}-${i}`);
                        res.status(500).end(JSON.stringify({
                            error: {
                                message: e.message
                            }
                        }));
                        console.timeEnd('testz3');
                        stopFlag = true;
                    });
    
                    subReq.write(postData);
                    subReq.end();
                })(i);
            }
        }
    };

    ftx(req, res);
});

router.post('/testz4', (req, res) => {
    console.time('testz4');
    console.log('POST /bill/api/v1.0/payment-systems/ecommpay/testz4');
    console.log(req.url);

    let ftx = (req, res) => {
        // Массив с объектами, включающими в себя идентификатор клиента, другие необходимы для выполнения теста данные и ответные данные ECommPay в том числе
        const data = req.body.data;

        let stopFlag = false;
        let numberOfQueriesExecuted = 0;
        for (let i = 0; i < data.length && !stopFlag; i++) {
            const currentItem = data[i];

            ((i) => {
                const clientId = currentItem.clientId;
                const paymentSystemId = currentItem.paymentSystemId;
                const amount = currentItem.amount;
                const subData = currentItem.subData;
                const clientOperationId = currentItem.clientOperationId;

                const urlx = config._qtbusEndpoint + '/?func=cash-in&params=' + JSON.stringify({
                    FClientId: clientId,
                    FPaymentSystemId: paymentSystemId,
                    FAmount: amount,
                    FSubData: subData,
                    FClientOperationId: clientOperationId
                });

                console.log(urlx);

                http.get(urlx, (subRes) => {
                    const { statusCode } = subRes;
                    const contentType = subRes.headers['content-type'];
                    let error = null;
                    if (statusCode !== 200) {
                        error = new Error(`Error query-${i}: Request Failed. Status Code: ${statusCode}`);
                    } else if (!/^application\/json/.test(contentType)) {
                        error = new Error(`Error query-${i}: Invalid content-type. Expected application/json but received ${contentType}`);
                    }

                    if (error) {
                        console.error(error.message);
                        subRes.resume();
                        return;
                    }

                    subRes.setEncoding('utf8');
                    let rawData = '';
                    subRes.on('data', (chunk) => { rawData += chunk; });
                    subRes.on('end', () => {
                        console.log(rawData);

                        try {
                            const parsedData = JSON.parse(rawData);

                            if (!parsedData['result'] || parsedData['result'] == false) {
                                res.status(500).end(JSON.stringify({
                                    error: {
                                        message: parsedData['error'] || 'Error'
                                    }
                                }));
                                console.error(parsedData['error'] || 'Error');
                                return;
                            }

                            console.log(parsedData['data']['url']);
                           
                            if (!parsedData['data']['url']) {
                                res.status(500).end(JSON.stringify({
                                    error: {
                                        message: parsedData['error'] || 'The received data does not contain the desired parameter - url.'
                                    }
                                }));
                                console.error(parsedData['error'] || 'The received data does not contain the desired parameter - url.');
                                return;
                            }

                            console.time(`query-${i}`);
                           
                            const postData = JSON.stringify(currentItem.paymentCallbackData);
    
                            const options = {
                                host: config.apiEndpoint.host,
                                port: config.apiEndpoint.port,
                                path: '/bill/api/v1.0/payment-systems/ecommpay',
                                method: 'POST',
                                headers: {
                                    'content-type': 'application/json',
                                    'content-length': Buffer.byteLength(postData)
                                }
                            };
            
                            const subReq2 = http.request(options, (subRes2) => {
                                const query = `${subRes2.req.method} ${subRes2.socket.remoteAddress}:${subRes2.socket.remotePort}${subRes2.req.path}`;
                                console.log(`query-${i}: query: ${query}`);
                                console.log(`query-${i}: status: ${subRes2.statusCode}`);
                                // console.log(`query-${i}: headers: ${JSON.stringify(subRes2.headers)}`);
                                subRes2.setEncoding('utf8');
                                let rawData = '';
                                subRes2.on('data', (chunk) => { rawData += chunk; });
                                subRes2.on('end', () => {
                                    console.log(rawData);
                                    console.log(`query-${i}: finished!`);
                                    console.timeEnd(`query-${i}`);

                                    if (++numberOfQueriesExecuted === data.length) {
                                        res.status(200).end();
                                        console.timeEnd('testz4');
                                    }
                                });
                            });
            
                            subReq2.on('error', (e) => {
                                console.error(`query-${i}: problem with request: ${e.message}`);
                                console.timeEnd(`query-${i}`);
                                res.status(500).end(JSON.stringify({
                                    error: {
                                        message: e.message
                                    }
                                }));
                                console.timeEnd('testz4');
                                stopFlag = true;
                            });
            
                            subReq2.write(postData);
                            subReq2.end();
                        } catch (e) {
                            console.error(e.message);
                        }
                    });
                }).on('error', (e) => {
                    console.error(`query-${i}: problem with request: ${e.message}`);
                    console.timeEnd(`query-${i}`);
                    res.status(500).end(JSON.stringify({
                        error: {
                            message: e.message
                        }
                    }));
                    console.timeEnd('testz4');
                    stopFlag = true;
                });
            })(i);
        }
    };

    ftx(req, res);
});

router.post('/testz5', (req, res) => {
    console.log('POST /bill/api/v1.0/payment-systems/ecommpay/testz5');
    console.log(req.url);

    res.connection.setTimeout(600000);

    let ftx = (req, res) => {
        let tempData = [];
        let requests = [];
        let completedRequests = [];

        const requestDiffInterval = setInterval(() => {
            let difference = requests
                .filter(x => !completedRequests.includes(x))
                .concat(completedRequests.filter(x => !requests.includes(x)));
            // console.log(JSON.stringify(difference));
            console.log(difference);
        }, 10000);

        let runTest = (cb) => {
            console.time('testz5');

            requests = [];
            completedRequests = [];

            let stopFlag = false;
            let numberOfQueriesExecuted = 0;
            for (let i = 0; i < tempData.length && !stopFlag; i++) {
                ((i) => {
                    let requestRetringCount = 0;
                    
                    let runRequest = () => {
                        console.time(`query-${i}`);

                        if (!requests.includes(i)) {
                            requests.push(i);
                        }

                        const currentItem = tempData[i];
                        const cUrl = url.parse(currentItem.url);
                        const params = new URLSearchParams(cUrl.query);
                        const paymentId = params.get('payment_id');
                        const customerId = params.get('customer_id');

                        let currentPayment = staticData.paymentCallbackData;
                        currentPayment.payment.id = paymentId;
                        currentPayment.customer.id = customerId;

                        let hash = crypto.createHash('md5');
                        hash.update((new Date()).toISOString());
                        let digest = hash.digest('hex');
                        currentPayment.account.token = digest + digest;

                        delete currentPayment.signature;
                        const signature = signer(currentPayment, config.ecommpay.salt);
                        currentPayment.signature = signature;

                        const postData = JSON.stringify(currentPayment);
                        console.log(`query-${i} postData:${postData}`);

                        const options = {
                            host: config.apiEndpoint.host,
                            port: config.apiEndpoint.port,
                            path: '/bill/api/v1.0/payment-systems/ecommpay',
                            method: 'POST',
                            headers: {
                                'content-type': 'application/json',
                                'content-length': Buffer.byteLength(postData)
                            }
                        };

                        const subReq2 = http.request(options, (subRes2) => {
                            const query = `${subRes2.req.method} ${subRes2.socket.remoteAddress}:${subRes2.socket.remotePort}${subRes2.req.path}`;
                            console.log(`query-${i}: query: ${query}`);
                            console.log(`query-${i}: status: ${subRes2.statusCode}`);
                            // console.log(`query-${i}: headers: ${JSON.stringify(subRes2.headers)}`);
                            subRes2.setEncoding('utf8');
                            let rawData = '';
                            subRes2.on('data', (chunk) => { rawData += chunk; });
                            subRes2.on('end', () => {
                                completedRequests.push(i);
                                console.timeEnd(`query-${i}`);

                                console.log(`numberOfQueriesExecuted: ${numberOfQueriesExecuted + 1}`);
                                if (++numberOfQueriesExecuted === tempData.length) {
                                    console.log('Test execution completed');
                                    res.status(200).end();
                                    console.timeEnd('testz5');
                                }
                            });
                        });

                        subReq2.on('error', (e) => {
                            console.error(`query-${i}: problem with request: ${e.message}`);
                            console.timeEnd(`query-${i}`);

                            if (requestRetringCount >= config.requestRetryCountLimit) {
                                const message = `Request query-${i}: retry limit reached. requestRetryCountLimit: ${config.requestRetryCountLimit}`;
                                console.error(`message: ${message}`);
                                subRes2.resume();
                                res.status(500).end(JSON.stringify({
                                    error: {
                                        message: message
                                    }
                                }));
                                console.timeEnd('testz5');
                                stopFlag = true;
                            }
                            else {
                                requestRetringCount++;
                                console.log(`Retring request query-${i}:... requestRetringCount: ${requestRetringCount}`);
                                runRequest();
                            }
                        });

                        subReq2.write(postData);
                        subReq2.end();
                    }

                    setTimeout(() => {
                        runRequest();
                    }, config.requestRunDelay);
                })(i);
            }
        }

        const staticData = req.body.staticData;
        const data = req.body.data;

        let stopFlag = false;
        let numberOfQueriesExecuted = 0;
        for (let i = 0; i < data.length && !stopFlag; i++) {
            const currentItem = data[i];
           
            ((i) => {
                const clientId = currentItem;
                const paymentSystemId = staticData.paymentSystemId;
                const amount = staticData.amount;
                const subData = staticData.subData;
                const clientOperationId = staticData.clientOperationId;

                const path = 
                    encodeURI('/?func=cash-in&params=' + JSON.stringify({
                        FClientId: clientId,
                        FPaymentSystemId: paymentSystemId,
                        FAmount: amount,
                        FSubData: subData,
                        FClientOperationId: clientOperationId
                    }));

                const options = {
                    host: config.qtbusEndpoint.host,
                    port: config.qtbusEndpoint.port,
                    path: path,
                    headers: {
                        'Connection': 'close'
                    },
                    timeout: 150000
                };

                console.log(`query-${i}: url: http://${options.host}:${options.port}${options.path}`);

                let requestRetringCount = 0;

                let runRequest = () => {
                    if (!requests.includes(i)) {
                        requests.push(i);
                    }

                    let request = http.get(options, (subRes) => {
                        const { statusCode } = subRes;
                        const contentType = subRes.headers['content-type'];
                        let error = null;
                        if (statusCode === 408) {
                            const error = new Error('Timeout');
                            request.emit('error', error);
                        } else if (statusCode !== 200) {
                            error = new Error(`Error query-${i}: Request Failed. Status Code: ${statusCode}`);
                        } else if (!/^application\/json/.test(contentType)) {
                            error = new Error(`Error query-${i}: Invalid content-type. Expected application/json but received ${contentType}`);
                        }
    
                        if (error) {
                            console.error(error.message);
                            subRes.resume();
                            // return;
                        }
    
                        subRes.setEncoding('utf8');
                        let rawData = '';
                        subRes.on('data', (chunk) => { rawData += chunk; });
                        subRes.on('end', () => {
                            completedRequests.push(i);
                            console.log(`query-${i}: response data: ${rawData}`);

                            try {
                                const parsedData = JSON.parse(rawData);

                                if (parsedData.hasOwnProperty('error') && parsedData['error'].hasOwnProperty('code') && parsedData['error']['code'] === 'ERR_SCRIPT_LOADIND') {
                                    // Повторный запрос в случае ошибки загрузки скрипта
                                    const error = new Error(parsedData['error']['message']);
                                    request.emit('error', error);
                                    return;
                                }

                                if (parsedData.hasOwnProperty('error') && parsedData['error'].hasOwnProperty('code') && parsedData['error']['code'] === 'ERR_SCRIPT_RUNTIME') {
                                    // Повторный запрос в случае ошибки выполнения скрипта
                                    const error = new Error(parsedData['error']['message']);
                                    request.emit('error', error);
                                    return;
                                }
    
                                if (!parsedData['result'] || parsedData['result'] == false) {
                                    res.status(500).end(JSON.stringify({
                                        error: {
                                            message: parsedData['error'] || 'Error'
                                        }
                                    }));
                                    console.error(parsedData['error'] || 'Error');
                                    return;
                                }
                               
                                if (!parsedData['data']['url']) {
                                    res.status(500).end(JSON.stringify({
                                        error: {
                                            message: parsedData['error'] || 'The received data does not contain the desired parameter - url.'
                                        }
                                    }));
                                    console.error(parsedData['error'] || 'The received data does not contain the desired parameter - url.');
                                    return;
                                }

                                // completedRequests.push(i); 
    
                                tempData.push({
                                    "url": parsedData['data']['url'],
                                    "clientId": clientId
                                });

                                console.log(`numberOfQueriesExecuted: ${numberOfQueriesExecuted + 1}`);
    
                                if (++numberOfQueriesExecuted === data.length) {
                                    // Обработчик для вызова после выполнения предварительной подготовки
                                    console.log('Test preparation is complete.\nRunning a test...');
                                    runTest();
                                }
                            } catch (e) {
                                console.error(e.message);
                            }
                        });
                    }).on('error', (e) => {
                        console.error(`query-${i}: problem with request: ${e.message}`); 
                        
                        if (requestRetringCount >= config.requestRetryCountLimit) {
                            const message = `Request query-${i}: retry limit reached. requestRetryCountLimit: ${config.requestRetryCountLimit}`;
                            console.error(`message: ${message}`);
                            res.status(500).end(JSON.stringify({
                                error: {
                                    message: message
                                }
                            }));
                            stopFlag = true;
                        }
                        else {
                            requestRetringCount++;
                            console.log(`Retring request query-${i}:... requestRetringCount: ${requestRetringCount}`);
                            runRequest();
                        }
                    });
                }

                setTimeout(() => {
                    runRequest();
                }, config.requestRunDelay);
            })(i);
        }
    }

    ftx(req, res);
});

router.post('/testz5m', (req, res) => {
    console.log('POST /bill/api/v1.0/payment-systems/ecommpay/testz5m');
    console.log(req.url);

    res.connection.setTimeout(600000);

    let ftx = (req, res) => {
        let tempData = [];
        let requests = [];
        let completedRequests = [];

        const requestDiffInterval = setInterval(() => {
            let difference = requests
                .filter(x => !completedRequests.includes(x))
                .concat(completedRequests.filter(x => !requests.includes(x)));
            // console.log(JSON.stringify(difference));
            console.log(difference);
        }, 10000);

        let runTest = (cb) => {
            console.time('testz5m');

            requests = [];
            completedRequests = [];

            let stopFlag = false;
            let numberOfQueriesExecuted = 0;
            
            const blockSize = 125;
            let startIndex = 0;
            let endIndex = blockSize;

            const testIntervalId = setInterval(() => {
                console.log('Executing a query group');
                console.log(`startIndex: ${startIndex}, endIndex: ${endIndex}`);
                if (endIndex > tempData.length) {
                    endIndex = Math.min(endIndex, tempData.length);
                    clearInterval(testIntervalId);
                }

                for (let i = startIndex; i < endIndex && !stopFlag; i++) {
                    ((i) => {
                        let requestRetringCount = 0;
                        
                        let runRequest = () => {
                            console.time(`query-${i}`);
    
                            if (!requests.includes(i)) {
                                requests.push(i);
                            }
    
                            const currentItem = tempData[i];
                            const cUrl = url.parse(currentItem.url);
                            const params = new URLSearchParams(cUrl.query);
                            const paymentId = params.get('payment_id');
                            const customerId = params.get('customer_id');
    
                            let currentPayment = staticData.paymentCallbackData;
                            currentPayment.payment.id = paymentId;
                            currentPayment.customer.id = customerId;
    
                            let hash = crypto.createHash('md5');
                            hash.update((new Date()).toISOString());
                            let digest = hash.digest('hex');
                            currentPayment.account.token = digest + digest;
    
                            delete currentPayment.signature;
                            const signature = signer(currentPayment, config.ecommpay.salt);
                            currentPayment.signature = signature;
    
                            const postData = JSON.stringify(currentPayment);
                            console.log(`query-${i} postData:${postData}`);
    
                            const options = {
                                host: config.apiEndpoint.host,
                                port: config.apiEndpoint.port,
                                path: '/bill/api/v1.0/payment-systems/ecommpay',
                                method: 'POST',
                                headers: {
                                    'content-type': 'application/json',
                                    'content-length': Buffer.byteLength(postData)
                                }
                            };
    
                            const subReq2 = http.request(options, (subRes2) => {
                                const query = `${subRes2.req.method} ${subRes2.socket.remoteAddress}:${subRes2.socket.remotePort}${subRes2.req.path}`;
                                console.log(`query-${i}: query: ${query}`);
                                console.log(`query-${i}: status: ${subRes2.statusCode}`);
                                // console.log(`query-${i}: headers: ${JSON.stringify(subRes2.headers)}`);
                                subRes2.setEncoding('utf8');
                                let rawData = '';
                                subRes2.on('data', (chunk) => { rawData += chunk; });
                                subRes2.on('end', () => {
                                    completedRequests.push(i);
                                    console.timeEnd(`query-${i}`);
    
                                    console.log(`numberOfQueriesExecuted: ${numberOfQueriesExecuted + 1}`);
                                    if (++numberOfQueriesExecuted === tempData.length) {
                                        console.log('Test execution completed');
                                        res.status(200).end();
                                        console.timeEnd('testz5m');
                                    }
                                });
                            });
    
                            subReq2.on('error', (e) => {
                                console.error(`query-${i}: problem with request: ${e.message}`);
                                console.timeEnd(`query-${i}`);
    
                                if (requestRetringCount >= config.requestRetryCountLimit) {
                                    const message = `Request query-${i}: retry limit reached. requestRetryCountLimit: ${config.requestRetryCountLimit}`;
                                    console.error(`message: ${message}`);
                                    subRes2.resume();
                                    res.status(500).end(JSON.stringify({
                                        error: {
                                            message: message
                                        }
                                    }));
                                    console.timeEnd('testz5m');
                                    stopFlag = true;
                                }
                                else {
                                    requestRetringCount++;
                                    console.log(`Retring request query-${i}:... requestRetringCount: ${requestRetringCount}`);
                                    runRequest();
                                }
                            });
    
                            subReq2.write(postData);
                            subReq2.end();
                        }
    
                        setTimeout(() => {
                            runRequest();
                        }, 0);
                    })(i);
                }

                startIndex += blockSize;
                endIndex += blockSize;

                console.log('Wait...');
            }, 30000);
        }

        const staticData = req.body.staticData;
        const data = req.body.data;

        let stopFlag = false;
        let numberOfQueriesExecuted = 0;
        for (let i = 0; i < data.length && !stopFlag; i++) {
            const currentItem = data[i];
           
            ((i) => {
                const clientId = currentItem;
                const paymentSystemId = staticData.paymentSystemId;
                const amount = staticData.amount;
                const subData = staticData.subData;
                const clientOperationId = staticData.clientOperationId;

                const path = 
                    encodeURI('/?func=cash-in&params=' + JSON.stringify({
                        FClientId: clientId,
                        FPaymentSystemId: paymentSystemId,
                        FAmount: amount,
                        FSubData: subData,
                        FClientOperationId: clientOperationId
                    }));

                const options = {
                    host: config.qtbusEndpoint.host,
                    port: config.qtbusEndpoint.port,
                    path: path,
                    headers: {
                        'Connection': 'close'
                    },
                    timeout: 150000
                };

                console.log(`query-${i}: url: http://${options.host}:${options.port}${options.path}`);

                let requestRetringCount = 0;

                let runRequest = () => {
                    if (!requests.includes(i)) {
                        requests.push(i);
                    }

                    let request = http.get(options, (subRes) => {
                        const { statusCode } = subRes;
                        const contentType = subRes.headers['content-type'];
                        let error = null;
                        if (statusCode === 408) {
                            const error = new Error('Timeout');
                            request.emit('error', error);
                        } else if (statusCode !== 200) {
                            error = new Error(`Error query-${i}: Request Failed. Status Code: ${statusCode}`);
                        } else if (!/^application\/json/.test(contentType)) {
                            error = new Error(`Error query-${i}: Invalid content-type. Expected application/json but received ${contentType}`);
                        }
    
                        if (error) {
                            console.error(error.message);
                            subRes.resume();
                            // return;
                        }
    
                        subRes.setEncoding('utf8');
                        let rawData = '';
                        subRes.on('data', (chunk) => { rawData += chunk; });
                        subRes.on('end', () => {
                            completedRequests.push(i);
                            console.log(`query-${i}: response data: ${rawData}`);

                            try {
                                const parsedData = JSON.parse(rawData);

                                if (parsedData.hasOwnProperty('error') && parsedData['error'].hasOwnProperty('code') && parsedData['error']['code'] === 'ERR_SCRIPT_LOADIND') {
                                    // Повторный запрос в случае ошибки загрузки скрипта
                                    const error = new Error(parsedData['error']['message']);
                                    request.emit('error', error);
                                    return;
                                }

                                if (parsedData.hasOwnProperty('error') && parsedData['error'].hasOwnProperty('code') && parsedData['error']['code'] === 'ERR_SCRIPT_RUNTIME') {
                                    // Повторный запрос в случае ошибки выполнения скрипта
                                    const error = new Error(parsedData['error']['message']);
                                    request.emit('error', error);
                                    return;
                                }
    
                                if (!parsedData['result'] || parsedData['result'] == false) {
                                    res.status(500).end(JSON.stringify({
                                        error: {
                                            message: parsedData['error'] || 'Error'
                                        }
                                    }));
                                    console.error(parsedData['error'] || 'Error');
                                    return;
                                }
                               
                                if (!parsedData['data']['url']) {
                                    res.status(500).end(JSON.stringify({
                                        error: {
                                            message: parsedData['error'] || 'The received data does not contain the desired parameter - url.'
                                        }
                                    }));
                                    console.error(parsedData['error'] || 'The received data does not contain the desired parameter - url.');
                                    return;
                                }

                                // completedRequests.push(i); 
    
                                tempData.push({
                                    "url": parsedData['data']['url'],
                                    "clientId": clientId
                                });

                                console.log(`numberOfQueriesExecuted: ${numberOfQueriesExecuted + 1}`);
    
                                if (++numberOfQueriesExecuted === data.length) {
                                    // Обработчик для вызова после выполнения предварительной подготовки
                                    console.log('Test preparation is complete.\nRunning a test...');
                                    runTest();
                                }
                            } catch (e) {
                                console.error(e.message);
                            }
                        });
                    }).on('error', (e) => {
                        console.error(`query-${i}: problem with request: ${e.message}`); 
                        
                        if (requestRetringCount >= config.requestRetryCountLimit) {
                            const message = `Request query-${i}: retry limit reached. requestRetryCountLimit: ${config.requestRetryCountLimit}`;
                            console.error(`message: ${message}`);
                            res.status(500).end(JSON.stringify({
                                error: {
                                    message: message
                                }
                            }));
                            stopFlag = true;
                        }
                        else {
                            requestRetringCount++;
                            console.log(`Retring request query-${i}:... requestRetringCount: ${requestRetringCount}`);
                            runRequest();
                        }
                    });
                }

                setTimeout(() => {
                    runRequest();
                }, config.requestRunDelay);
            })(i);
        }
    }

    ftx(req, res);
});

router.get('/processing-orders-for-out', (req, res) => {
    console.log('GET /bill/api/v1.0/payment-systems/ecommpay/processing-orders-for-out');
    console.log(req.url);

    let ftx = (req, res) => {
        let runRequestRecieveOrdersForOut = (cb) => {
            let requestRetringCount = 0;

            const path = 
                encodeURI('/?func=recieve-orders-for-out&params=' + JSON.stringify({}));

            const options = {
                host: config.qtbusEndpoint.host,
                port: config.qtbusEndpoint.port,
                path: path,
                headers: {
                    'Connection': 'close'
                },
                timeout: 150000
            };

            console.log(`url: http://${options.host}:${options.port}${options.path}`);

            let runRequest = (cb) => {
                http.get(options, (subRes) => {
                    const { statusCode } = subRes;
                    const contentType = subRes.headers['content-type'];
                    let error = null;
                    if (statusCode === 408) {
                        const error = new Error('Timeout');
                        request.emit('error', error);
                    } else if (statusCode !== 200) {
                        error = new Error(`runRequestRecieveOrdersForOut: Request Failed. Status Code: ${statusCode}`);
                    } else if (!/^application\/json/.test(contentType)) {
                        error = new Error(`runRequestRecieveOrdersForOut: Invalid content-type. Expected application/json but received ${contentType}`);
                    }

                    if (error) {
                        subRes.resume();
                        cb && cb({
                            error: {
                                message: error.message
                            }
                        }, null);
                        return;
                    }

                    subRes.setEncoding('utf8');
                    let rawData = '';
                    subRes.on('data', (chunk) => { rawData += chunk; });
                    subRes.on('end', () => {
                        console.log(`runRequestRecieveOrdersForOut: response data: ${rawData}`);

                        try {
                            const parsedData = JSON.parse(rawData);

                            if (parsedData.hasOwnProperty('error') && parsedData['error'].hasOwnProperty('code') && parsedData['error']['code'] === 'ERR_SCRIPT_LOADIND') {
                                // Повторный запрос в случае ошибки загрузки скрипта
                                const error = new Error(parsedData['error']['message']);
                                request.emit('error', error);
                                return;
                            }

                            if (parsedData.hasOwnProperty('error') && parsedData['error'].hasOwnProperty('code') && parsedData['error']['code'] === 'ERR_SCRIPT_RUNTIME') {
                                // Повторный запрос в случае ошибки выполнения скрипта
                                const error = new Error(parsedData['error']['message']);
                                request.emit('error', error);
                                return;
                            }

                            if (!parsedData['result'] || parsedData['result'] == false) {
                                const message = parsedData['error'] || 'Error';
                                cb && cb({
                                    error: {
                                        message: message
                                    }
                                }, null);
                                return;
                            }
                            
                            const orders = parsedData['data'] || [];
                            if (orders.length === 0) {
                                cb && cb(
                                    null,
                                    {
                                        status: 200,
                                        message: ''
                                    }
                                );
                                return;
                            }
                            processResponseRecieveOrdersForOut(orders, cb);
                        } catch (e) {
                            console.error(e.message);
                        }
                    });
                }).on('error', (e) => {
                    console.error(`runRequestRecieveOrdersForOut: problem with request: ${e.message}`);

                    if (requestRetringCount >= config.requestRetryCountLimit) {
                        const message = `runRequestRecieveOrdersForOut: retry limit reached. requestRetryCountLimit: ${config.requestRetryCountLimit}`;
                        cb && cb({
                            error: {
                                message: message
                            }
                        }, null);
                    }
                    else {
                        requestRetringCount++;
                        console.log(`Retring runRequestRecieveOrdersForOut:... requestRetringCount: ${requestRetringCount}`);
                        runRequest(cb);
                    }
                });
            }

            runRequest(cb);
        }

        runRequestRecieveOrdersForOut((error, data) => {
            if (error) {
                console.error(error);
                res.status(500).end(JSON.stringify(error));
                return;
            }

            res.status(data.status).end(data.message);
        });

        let processResponseRecieveOrdersForOut = (orders, cb) => {
            for (let i = 0; i < orders.length; i++) {
                const currentOrder = orders[i];
                runRequestWriteOffsClientBalanceGetExtId(currentOrder, cb);
            }
        }

        let runRequestWriteOffsClientBalanceGetExtId = (currentOrder, cb) => {
            const path = 
                encodeURI('/?func=write-offs-client-balance&params=' + JSON.stringify({
                    'app_call': 'Get_ExtId',
                    'payment_system_id': 150,
                    'order_id': currentOrder['orderId'],
                    'client_id': currentOrder['clientId'],
                    'session': {}
                }));

            const options = {
                host: config.qtbusEndpoint.host,
                port: config.qtbusEndpoint.port,
                path: path,
                headers: {
                    'Connection': 'close'
                },
                timeout: 150000
            };

            console.log(`url: http://${options.host}:${options.port}${options.path}`);

            let requestRetringCount = 0;
            
            let runRequest = (cb)  => {
                console.time(`runRequestWriteOffsClientBalanceGetExtId`);

                http.get(options, (subRes) => {
                    const { statusCode } = subRes;
                    const contentType = subRes.headers['content-type'];
                    let error = null;
                    if (statusCode === 408) {
                        const error = new Error('Timeout');
                        request.emit('error', error);
                    } else if (statusCode !== 200) {
                        error = new Error(`runRequestWriteOffsClientBalanceGetExtId: Request Failed. Status Code: ${statusCode}`);
                    } else if (!/^application\/json/.test(contentType)) {
                        error = new Error(`runRequestWriteOffsClientBalanceGetExtId: Invalid content-type. Expected application/json but received ${contentType}`);
                    }
    
                    if (error) {
                        subRes.resume();
                        console.timeEnd(`runRequestWriteOffsClientBalanceGetExtId`);
                        cb && cb({
                            error: {
                                message: error.message
                            }
                        }, null);
                        return;
                    }
    
                    subRes.setEncoding('utf8');
                    let rawData = '';
                    subRes.on('data', (chunk) => { rawData += chunk; });
                    subRes.on('end', () => {
                        console.timeEnd(`runRequestWriteOffsClientBalanceGetExtId`);
                        console.log(`runRequestWriteOffsClientBalanceGetExtId: response data: ${rawData}`);

                        try {
                            const parsedData = JSON.parse(rawData);

                            if (parsedData.hasOwnProperty('error') && parsedData['error'].hasOwnProperty('code') && parsedData['error']['code'] === 'ERR_SCRIPT_LOADIND') {
                                // Повторный запрос в случае ошибки загрузки скрипта
                                const error = new Error(parsedData['error']['message']);
                                request.emit('error', error);
                                return;
                            }

                            if (parsedData.hasOwnProperty('error') && parsedData['error'].hasOwnProperty('code') && parsedData['error']['code'] === 'ERR_SCRIPT_RUNTIME') {
                                // Повторный запрос в случае ошибки выполнения скрипта
                                const error = new Error(parsedData['error']['message']);
                                request.emit('error', error);
                                return;
                            }
    
                            if (!parsedData['result'] || parsedData['result'] == false) {
                                const message = parsedData['error'] || 'Error';
                                cb && cb({
                                    error: {
                                        message: message
                                    }
                                }, null);
                                return;
                            }

                            runRequestEcommpayPayout(parsedData, currentOrder, cb);
                        } catch (e) {
                            console.error(e.message);
                        }
                    });
                }).on('error', (e) => {
                    console.error(`runRequestWriteOffsClientBalanceGetExtId: problem with request: ${e.message}`);
                
                    if (requestRetringCount >= config.requestRetryCountLimit) {
                        console.timeEnd(`runRequestWriteOffsClientBalanceGetExtId`);
                        const message = `runRequestWriteOffsClientBalanceGetExtId: retry limit reached. requestRetryCountLimit: ${config.requestRetryCountLimit}`;
                        cb && cb({
                            error: {
                                message: message
                            }
                        }, null);
                    }
                    else {
                        requestRetringCount++;
                        console.log(`Retring runRequestWriteOffsClientBalanceGetExtId:... requestRetringCount: ${requestRetringCount}`);
                        runRequest(cb);
                    }
                });
            }

            runRequest(cb);
        }

        let runRequestEcommpayPayout = (parsedData, currentOrder, cb) => {
            let session = JSON.parse(parsedData['data']['session']);
            let dataPS = session['SESS_DT'];

            let urlPS = '';
            for (let k = 0; k < dataPS.length; k++) {
                const currentItem = dataPS[k];
                if (currentItem.hasOwnProperty('HTTP')) {
                    urlPS = currentItem['HTTP']['url'];
                    break;
                }
            }
            
            let requestPS = {};
            for (let k = 0; k < dataPS.length; k++) {
                const currentItem = dataPS[k];
                if (currentItem.hasOwnProperty('REQUEST')) {
                    requestPS = currentItem['REQUEST'];
                    break;
                }
            }

            let orderDTPS = {};
            for (let k = 0; k < dataPS.length; k++) {
                const currentItem = dataPS[k];
                if (currentItem.hasOwnProperty('ORDER_DT')) {
                    orderDTPS = currentItem['ORDER_DT'];
                    break;
                }
            }

            // NOTE Коррекция данных для того, чтобы ecommpay sdk корректно построил запрос, т. к. значение null приводит к ошибке
            requestPS['token'] = (requestPS['token'] === null || requestPS['token'] === undefined) ? {} : requestPS['token'];
            
            delete requestPS.general.signature;
            const signature = signer(requestPS, config.ecommpay.salt);
            requestPS.general.signature = signature;

            const urlx = urlPS;
            console.log(`urlx: ${urlx}`);
            
            let requestRetringCount = 0;

            let runRequest = (cb) => {
                console.time(`runRequestEcommpayPayout`);
            
                const cUrl = url.parse(urlx);
                const postData = JSON.stringify(requestPS);

                const options = {
                    host: cUrl.host,
                    port: cUrl.port,
                    path: cUrl.path,
                    method: 'POST',
                    headers: {
                        'content-type': 'application/json',
                        'content-length': Buffer.byteLength(postData)
                    }
                };

                const subReq = https.request(options, (subRes) => {
                    const query = `${subRes.req.method} ${subRes.socket.remoteAddress}:${subRes.socket.remotePort}${subRes.req.path}`;
                    console.log(`runRequestEcommpayPayout: query: ${query}`);
                    console.log(`runRequestEcommpayPayout: status: ${subRes.statusCode}`);
                    // console.log(`query: headers: ${JSON.stringify(subRes.headers)}`);
                    subRes.setEncoding('utf8');
                    let rawData = '';
                    subRes.on('data', (chunk) => { rawData += chunk; });
                    subRes.on('end', () => {
                        console.timeEnd(`runRequestEcommpayPayout`);
                        console.log(`runRequestEcommpayPayout: response data: ${rawData}`);

                        const parsedData = JSON.parse(rawData);
                        
                        const { statusCode } = subRes;
                        const contentType = subRes.headers['content-type'];
                        let error = null;
                        if (!/^application\/json/.test(contentType)) {
                            error = new Error(`runRequestEcommpayPayout: Invalid content-type. Expected application/json but received ${contentType}`);
                        }

                        if (error) {
                            subRes.resume();
                            console.timeEnd(`runRequestEcommpayPayout`);
                            cb && cb({
                                error: {
                                    message: error.message
                                }
                            }, null);
                            return;
                        }

                        processResponseEcommpayPayout(statusCode, parsedData, currentOrder, orderDTPS, cb);
                    });
                });

                subReq.on('error', (e) => {
                    console.error(`runRequestEcommpayPayout: problem with request: ${e.message}`);

                    if (requestRetringCount >= config.requestRetryCountLimit) {
                        console.timeEnd(`runRequestEcommpayPayout`);
                        const message = `runRequestEcommpayPayout: retry limit reached. requestRetryCountLimit: ${config.requestRetryCountLimit}`;
                        cb && cb({
                            error: {
                                message: message
                            }
                        }, null);
                        subRes.resume();
                    }
                    else {
                        requestRetringCount++;
                        console.log(`Retring runRequestEcommpayPayout:... requestRetringCount: ${requestRetringCount}`);
                        runRequest(cb);
                    }
                });

                subReq.write(postData);
                subReq.end();
            }
           
            runRequest(cb);
        }

        let processResponseEcommpayPayout = (statusCode, parsedData, currentOrder, orderDTPS, cb) => {
            let responseCode = 0;
            let responseMessage = '';
            if (statusCode === 200) {
                responseCode = 200;
                responseMessage = parsedData.status;
            } else if (statusCode === 400 || statusCode === 403 || statusCode === 422 || statusCode === 500) {
                responseCode = parsedData.code;
                responseMessage = parsedData.message;
            }

            orderDTPS['RespCode'] = responseCode;
            orderDTPS['RespText'] = responseMessage;

            runRequestWriteOffsClientBalanceConfirm(currentOrder, orderDTPS, cb);
        }

        let runRequestWriteOffsClientBalanceConfirm = (currentOrder, orderDTPS, cb) => {
            const path = 
                encodeURI('/?func=write-offs-client-balance&params=' + JSON.stringify({
                    'app_call': 'Confirm',
                    'payment_system_id': 150,
                    'order_id': currentOrder['orderId'],
                    'client_id': currentOrder['clientId'],
                    'session': {
                        'ORDER_DT': orderDTPS
                    }
                }));

            const options = {
                host: config.qtbusEndpoint.host,
                port: config.qtbusEndpoint.port,
                path: path,
                headers: {
                    'Connection': 'close'
                },
                timeout: 150000
            };

            console.log(`url: http://${options.host}:${options.port}${options.path}`);

            let requestRetringCount = 0;

            let runRequest = (cb) => {
                console.time(`runRequestWriteOffsClientBalanceConfirm`);

                http.get(options, (subRes) => {
                    const { statusCode } = subRes;
                    const contentType = subRes.headers['content-type'];
                    let error = null;
                    if (statusCode === 408) {
                        const error = new Error('Timeout');
                        request.emit('error', error);
                    } else if (statusCode !== 200) {
                        error = new Error(`runRequestWriteOffsClientBalanceConfirm: Request Failed. Status Code: ${statusCode}`);
                    } else if (!/^application\/json/.test(contentType)) {
                        error = new Error(`runRequestWriteOffsClientBalanceConfirm: Invalid content-type. Expected application/json but received ${contentType}`);
                    }
    
                    if (error) {
                        subRes.resume();
                        console.timeEnd(`runRequestWriteOffsClientBalanceConfirm`);
                        cb && cb({
                            error: {
                                message: error.message
                            }
                        }, null);
                        return;
                    }
    
                    subRes.setEncoding('utf8');
                    let rawData = '';
                    subRes.on('data', (chunk) => { rawData += chunk; });
                    subRes.on('end', () => {
                        console.timeEnd(`runRequestWriteOffsClientBalanceConfirm`);
                        console.log(`runRequestWriteOffsClientBalanceConfirm: response data: ${rawData}`);

                        try {
                            const parsedData = JSON.parse(rawData);
                            
                            if (parsedData.hasOwnProperty('error') && parsedData['error'].hasOwnProperty('code') && parsedData['error']['code'] === 'ERR_SCRIPT_LOADIND') {
                                // Повторный запрос в случае ошибки загрузки скрипта
                                const error = new Error(parsedData['error']['message']);
                                request.emit('error', error);
                                return;
                            }

                            if (parsedData.hasOwnProperty('error') && parsedData['error'].hasOwnProperty('code') && parsedData['error']['code'] === 'ERR_SCRIPT_RUNTIME') {
                                // Повторный запрос в случае ошибки выполнения скрипта
                                const error = new Error(parsedData['error']['message']);
                                request.emit('error', error);
                                return;
                            }
    
                            if (!parsedData['result'] || parsedData['result'] == false) {
                                const message = parsedData['error'] || 'Error';
                                cb && cb({
                                    error: {
                                        message: message
                                    }
                                }, null);
                                return;
                            }

                            cb && cb(
                                null,
                                {
                                    status: 200,
                                    message: ''
                                }
                            );
                            
                        } catch (e) {
                            console.error(e.message);
                        }
                    });
                }).on('error', (e) => {
                    console.error(`runRequestWriteOffsClientBalanceConfirm: problem with request: ${e.message}`);
                
                    if (requestRetringCount >= config.requestRetryCountLimit) {
                        console.timeEnd(`runRequestWriteOffsClientBalanceConfirm`);
                        const message = `runRequestWriteOffsClientBalanceConfirm: retry limit reached. requestRetryCountLimit: ${config.requestRetryCountLimit}`;
                        cb && cb({
                            error: {
                                message: message
                            }
                        }, null);
                    }
                    else {
                        requestRetringCount++;
                        console.log(`Retring runRequestWriteOffsClientBalanceConfirm:... requestRetringCount: ${requestRetringCount}`);
                        runRequest(cb);
                    }
                });
            }

            runRequest(cb);
        }
    }

    ftx(req, res);
});

module.exports = router;
