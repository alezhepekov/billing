const signer = require('ecommpay/src/signer');

class Callback {
    constructor(salt, data) {
        this.responseData = typeof data === 'string' ? JSON.parse(data) : data;
        this.salt = salt;
        this.signature = this.responseData.signature;
        delete this.responseData.signature;       
    }

    isValid() {        
        return signer(this.responseData, this.salt) === this.signature;
    }  
}

module.exports = Callback;
