#!/bin/bash

function abspath() {
    if [ -d "$1" ]; then
        # dir
        (cd "$1"; pwd)
    elif [ -f "$1" ]; then
        # file
        if [[ $1 = /* ]]; then
            echo "$1"
        elif [[ $1 == */* ]]; then
            echo "$(cd "${1%/*}"; pwd)/${1##*/}"
        else
            echo "$(pwd)/$1"
        fi
    fi
}

function dirname() {
    echo "$(cd "${1%/*}"; pwd)/"
}

function usage() {
    echo "Script for building images for service BILLING"
    echo
    echo "Usage:"
    echo
    echo "sudo ./deploy <command> <tag> <db tag> <mq tag>"
    echo
    echo "<command> - what we need to do"
    echo
    echo "    build   - build images for local development"
    echo "    branch  - build images and push to registry for deploy on branch on test"
    echo "    release - build images and push to registry for deploy on test and production"
    echo
    echo "<tag>    release  tag version (default latest)"
    echo "<db tag> database tag version (default latest)"
    echo "<mq tag> rabbitmq tag version (default latest)"

    echo
    echo "sudo ./deploy/build.sh build   7.0.12"
    echo "sudo ./deploy/build.sh branch  7.0.12"
    echo "sudo ./deploy/build.sh release 7.0.12"
    echo
    echo "sudo ./deploy/build.sh branch  7.0.12 16.1.0"
    echo "sudo ./deploy/build.sh release 7.0.12 16.1.0 17.0.0"
    echo "sudo ./deploy/build.sh build   7.0.12 16.1.0 17.0.0"
    echo
}

